use anyhow::{bail, Result};
use current_locale::current_locale;
use log::{info, warn};
use serde::{Deserialize, Serialize};
use std::io::{stdin, stdout, BufRead, Write};
use std::path::PathBuf;
use structopt::StructOpt;

const PROGRAM_NAME: &str = env!("CARGO_BIN_NAME");
const DEFAULT_API_URL: &str = "https://api.timelimit.io/";

fn project_dirs() -> directories::ProjectDirs {
    directories::ProjectDirs::from("net", "silwol", "limitr").unwrap()
}

fn query_user_input(msg: &str) -> String {
    print!("{}", msg);

    stdout().flush().ok();

    let stdin = stdin();
    let mut lines = stdin.lock().lines();
    lines.next().unwrap().unwrap()
}

#[derive(Serialize, Deserialize)]
struct Config {
    api_url: String,
    email: String,
}

impl Config {
    fn store(&self) -> Result<()> {
        let toml_string = toml::to_string(self)?;

        std::fs::create_dir_all(Self::dir_path())?;
        std::fs::write(Self::file_path(), &toml_string)?;

        Ok(())
    }

    fn load() -> Result<Self> {
        let s = std::fs::read_to_string(Self::file_path())?;
        let loaded: Self = toml::from_str(&s)?;
        Ok(loaded)
    }

    fn dir_path() -> PathBuf {
        project_dirs().config_dir().to_path_buf()
    }

    fn file_path() -> PathBuf {
        Self::dir_path().join("config.toml")
    }
}

#[derive(Serialize, Deserialize)]
struct LocalData {
    auth_token: String,
}

impl LocalData {
    fn store(&self) -> Result<()> {
        let toml_string = toml::to_string(self)?;

        std::fs::create_dir_all(Self::dir_path())?;
        std::fs::write(Self::file_path(), &toml_string)?;

        Ok(())
    }

    fn load() -> Result<Self> {
        let s = std::fs::read_to_string(Self::file_path())?;
        let loaded: Self = toml::from_str(&s)?;
        Ok(loaded)
    }

    fn dir_path() -> PathBuf {
        project_dirs().data_dir().to_path_buf()
    }

    fn file_path() -> PathBuf {
        Self::dir_path().join("data.toml")
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let log_env = std::env::var("RUST_LOG").ok();
    let show_module_path = log_env.is_some();
    let level = "info";

    let filters = log_env.unwrap_or(format!(
        "{program_name}={level},limitr_webclient={level}",
        program_name = PROGRAM_NAME.replace('-', "_"),
        level = level
    ));
    env_logger::Builder::new()
        .format_timestamp(None)
        .parse_filters(&filters)
        .format_module_path(show_module_path)
        .init();

    let command = Command::from_args();

    command.process().await
}

#[derive(StructOpt)]
enum Command {
    /// Initialize the command-line tool
    Initialize {
        email: String,

        #[structopt(long, default_value = DEFAULT_API_URL)]
        api_url: String,
    },
    /// Authenticate against the TimeLimit server
    Authenticate,
}

impl Command {
    async fn process(self) -> Result<()> {
        match self {
            Command::Initialize { api_url, email } => {
                if Config::load().is_ok() {
                    bail!(
                        concat!(
                            "Already initialized.\n",
                            "If you'd like to reinitialize, delete the ",
                            "config file {:?}, then try again."
                        ),
                        Config::file_path()
                    );
                } else {
                    info!("Initializing with server {}", api_url);

                    let config = Config { api_url, email };
                    config.store()?;

                    info!("Config file {:?} written, you can now authenticate with `{} authenticate`",
                        Config::file_path(), PROGRAM_NAME);

                    Ok(())
                }
            }
            Command::Authenticate => {
                if let Ok(config) = Config::load() {
                    info!("Authenticating to server {}", config.api_url);
                    let locale = current_locale().unwrap_or_else(|e| {
                        warn!("{}", e);
                        "en".to_string()
                    });

                    let token = limitr_webclient::request_auth(
                        &config.api_url,
                        &config.email,
                        &locale,
                    )
                    .await?;

                    let code = query_user_input(
                        "Enter code received by e-mail: ",
                    );
                    let response = limitr_webclient::finish_auth(
                        &config.api_url,
                        &code,
                        &token.mail_login_token,
                    )
                    .await?;
                    let data = LocalData {
                        auth_token: response.mail_auth_token,
                    };
                    data.store()?;

                    info!("Successfully authenticated.");

                    Ok(())
                } else {
                    bail!("Config file {:?} not found, run `{} initialize` first.", Config::file_path(), PROGRAM_NAME);
                }
            }
        }
    }
}
