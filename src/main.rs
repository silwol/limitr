use logind_zbus::ManagerProxy;
use zbus::Connection;

fn main() {
    let connection = Connection::new_system().unwrap();
    let manager = ManagerProxy::new(&connection).unwrap();

    println!("{:#?}", manager.list_sessions().unwrap());
}
