use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct SendMailLoginCodeRequest {
    pub mail: String,
    pub locale: String,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct SendMailLoginCodeResponse {
    pub mail_login_token: String,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct SignInByMailCodeRequest {
    pub received_code: String,
    pub mail_login_token: String,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct SignInByMailCodeResponse {
    pub mail_auth_token: String,
}
