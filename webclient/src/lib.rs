mod error;
mod model;

pub use error::Error;
pub type Result<T, E = Error> = std::result::Result<T, E>;

pub async fn request_auth(
    base_url: &str,
    mail: &str,
    locale: &str,
) -> Result<model::SendMailLoginCodeResponse> {
    let url = format!("{}/auth/send-mail-login-code-v2", base_url);
    let data = model::SendMailLoginCodeRequest {
        mail: mail.to_string(),
        locale: locale.to_string(),
    };

    let client = reqwest::Client::new();
    let res = client.post(&url).json(&data).send().await?;

    Ok(res.json().await?)
}

pub async fn finish_auth(
    base_url: &str,
    received_code: &str,
    mail_login_token: &str,
) -> Result<model::SignInByMailCodeResponse> {
    let url = format!("{}/auth/sign-in-by-mail-code", base_url);
    let data = model::SignInByMailCodeRequest {
        received_code: received_code.to_string(),
        mail_login_token: mail_login_token.to_string(),
    };

    let client = reqwest::Client::new();
    let res = client.post(&url).json(&data).send().await?;
    Ok(res.json().await?)
}
