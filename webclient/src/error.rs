use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Reqwest error: {:?}", source)]
    ReqwestError {
        #[from]
        source: reqwest::Error,
    },
}
