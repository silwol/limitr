#[get("/")]
pub(crate) async fn index() -> Result<impl Responder> {
    let template = IndexTemplate {};
    let render = web::block(move || template.render()).await?;

    Ok(HttpResponse::Ok().content_type("text/html").body(render))
}

#[derive(Debug, PartialEq, Template)]
#[template(path = "index.html")]
struct IndexTemplate {}

use crate::Result;
use actix_web::{get, web, HttpResponse, Responder};
use askama::Template;
