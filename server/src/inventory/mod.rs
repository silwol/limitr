use crate::{Error, Result};
use argon2::{Argon2, PasswordHash, PasswordHasher, PasswordVerifier};
use rand::Rng;
use std::collections::BTreeMap;

#[derive(Default)]
pub struct Inventory {
    users: BTreeMap<String, User>,
}

impl Inventory {
    pub fn user(&self, username: &str) -> Result<&User> {
        self.users
            .get(username)
            .ok_or_else(|| Error::UsernameNotFound {
                username: username.to_string(),
            })
    }

    pub fn create_user(
        &mut self,
        username: &str,
        human_name: &str,
        password: &str,
    ) -> Result<()> {
        let username = username.to_string();
        if self.users.contains_key(&username) {
            return Err(Error::UsernameAlreadyExists {
                username: username.to_string(),
            });
        }
        let argon2 = Argon2::default();
        let salt: String = rand::thread_rng()
            .sample_iter(&rand::distributions::Alphanumeric)
            .take(50)
            .map(char::from)
            .collect();
        let argon2_password =
            argon2.hash_password(password.as_ref(), &salt)?.to_string();

        let user = User {
            human_name: human_name.to_string(),
            argon2_password,
        };

        self.users.insert(username, user);
        Ok(())
    }

    pub fn authenticate_user(
        &self,
        username: &str,
        password: Option<&str>,
    ) -> Result<()> {
        let password =
            password.ok_or_else(|| Error::AuthorizationFailed)?;
        let user = self
            .users
            .get(username)
            .ok_or_else(|| Error::AuthorizationFailed)?;
        let argon2 = Argon2::default();
        let hash = PasswordHash::new(&user.argon2_password)
            .map_err(|_| Error::AuthorizationFailed)?;
        argon2
            .verify_password(password.as_ref(), &hash)
            .map_err(|_| Error::AuthorizationFailed)?;
        Ok(())
    }
}

pub struct User {
    human_name: String,
    argon2_password: String,
}

impl User {}
