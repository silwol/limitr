mod pages;

use actix_web::{
    dev::ServiceRequest, get, middleware, web, App, HttpResponse,
    HttpServer, Responder,
};
use actix_web_httpauth::{
    extractors::basic::BasicAuth, middleware::HttpAuthentication,
};
use std::sync::{Arc, RwLock};

mod error;
mod inventory;

use error::Error;
use inventory::Inventory;

type Result<T, E = Error> = std::result::Result<T, E>;

#[get("")]
async fn example() -> impl Responder {
    HttpResponse::Ok().body("example")
}

type LockedInventory = Arc<RwLock<Inventory>>;

async fn validate(
    request: ServiceRequest,
    credentials: BasicAuth,
) -> Result<ServiceRequest, actix_web::Error> {
    use derive_more::{Display, Error};
    #[derive(Debug, Display, Error)]
    #[display(fmt = "authentication error")]
    struct AuthError;
    impl actix_web::error::ResponseError for AuthError {}

    {
        let inventory = request.app_data::<LockedInventory>().unwrap();
        let inventory = inventory.read().unwrap();
        inventory
            .authenticate_user(
                credentials.user_id(),
                credentials.password().map(|s| s.as_ref()),
            )
            .map_err(|_| AuthError)?;
    }
    Ok(request)
}

#[actix_web::main]
async fn main() -> Result<()> {
    let inventory = LockedInventory::default();

    inventory.write().unwrap().create_user(
        "hello",
        "Hello User",
        "password",
    )?;

    std::env::set_var("RUST_LOG", "actix_web=debug");
    env_logger::init();

    Ok(HttpServer::new(move || {
        let inventory: LockedInventory = inventory.clone();
        let auth = HttpAuthentication::basic(validate);
        App::new()
            .app_data(inventory)
            .wrap(middleware::Logger::default())
            .wrap(middleware::NormalizePath::new(
                middleware::normalize::TrailingSlash::Trim,
            ))
            .service(pages::index)
            .service(web::scope("app").wrap(auth).service(example))
    })
    .bind("[::]:4772")?
    .run()
    .await?)
}
