use actix_web::{error::BlockingError, http, HttpResponse};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Template rendering failed")]
    TemplateRendering {
        #[from]
        source: BlockingError<askama::Error>,
    },

    #[error("Username {:?} already exists", username)]
    UsernameAlreadyExists { username: String },

    #[error("Username {:?} not found", username)]
    UsernameNotFound { username: String },

    #[error("Password generation error: {:?}", source)]
    PasswordGenerationError {
        #[from]
        source: argon2::Error,
    },

    #[error("Password hash error: {:?}", source)]
    PasswordHashError {
        #[from]
        source: argon2::password_hash::Error,
    },

    #[error("Authorization failed")]
    AuthorizationFailed,

    #[error("I/O error: {:?}", source)]
    IOError {
        #[from]
        source: std::io::Error,
    },
}

impl actix_web::error::ResponseError for Error {
    fn error_response(&self) -> HttpResponse {
        actix_http::ResponseBuilder::new(self.status_code())
            .set_header(
                http::header::CONTENT_TYPE,
                "text/plain; charset=utf-8",
            )
            .body(format!(
                "Error {}\n{}",
                self.status_code(),
                self.to_string()
            ))
    }

    fn status_code(&self) -> http::StatusCode {
        // TODO: choose different status codes depending
        // on error
        http::StatusCode::INTERNAL_SERVER_ERROR
    }
}
